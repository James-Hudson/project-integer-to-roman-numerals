<?php

namespace App;

class RomanNumeralsMapper
{
	private static $roman_numerals = array(
                'M'  => 1000,
                'CM' => 900,
                'D'  => 500,
                'CD' => 400,
                'C'  => 100,
                'XC' => 90,
                'L'  => 50,
                'XL' => 40,
                'X'  => 10,
                'IX' => 9,
                'V'  => 5,
                'IV' => 4,
                'I'  => 1);


	public static function map($int)
	{
		$res = "";

		foreach (self::$roman_numerals as $roman => $number) 
		{

        	$matches = intval($int / $number);
 
        	$res .= str_repeat($roman, $matches);

        	$int = $int % $number;
    	}
 
    	/*** return the res ***/
    	return $res;
	}
}